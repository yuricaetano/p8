def post_transaction(filename, title, transaction_type, value):
    with open(filename, 'a') as file:

        title = str('title: ' + str(title) + '\n' )        
        file.write(title)

        transaction_type = str('transaction_type: ' + str(transaction_type) + '\n' )        
        file.write(transaction_type)

        value = str('value: ' + str(value) + '\n' )        
        file.write(value)
        
        tracejado = ("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" + '\n' )
        file.write(tracejado)

def all_transactions(filename):
    tracejado = ("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
    result = []

    with open(filename) as transactions:
        my_dict = dict()
        for line in transactions.readlines():
            line = line.replace("\n", "")
            line = line.split(": ")

            if(line[0] == tracejado):
                result.append(my_dict)
                my_dict = dict()
                continue

            my_dict[line[0]] = line[1]

    return result

def make_appointment(filename, name, hour, description):
    id_number = 1

    with open(filename, 'r+') as file:
        for line in file:
            line = line.split(": ")
            if(line[0] == 'id'):
                id_number = float(line[1].replace('\n', ""))
                id_number += 1
                id_number = int(id_number)

        id_ticket = str('id: ' + str(id_number) + '\n' )        
        file.write(id_ticket)

        name = str('name: ' + str(name) + '\n' )        
        file.write(name)

        hour = str('hour: ' + str(hour) + '\n' )        
        file.write(hour)

        description = str('description: ' + str(description) + '\n' )        
        file.write(description)
        
        tracejado = ("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" + '\n' )
        file.write(tracejado)

def time_is_free(filename, hour):
    with open(filename, 'r+') as file:
        for line in file:
            line = line.split(": ")
            if(line[0] == 'hour'):
                hour_placed = line[1].replace('\n', "")
                if(hour_placed == hour):
                    return False
                continue
    return True