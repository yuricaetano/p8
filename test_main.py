from main import post_transaction, all_transaction, make_appointment, time_is_free

def test_post_transaction():

    filename = "transactions.txt"
    title = "supermarket purchases"
    transaction_type = "outcome"
    value = "400.85"
    post_transaction(filename, title, transaction_type, value)

    filename = "transactions.txt"
    title = "salary"
    transaction_type = "income"
    value = "3000"
    post_transaction(filename, title, transaction_type, value)

    filename = "transactions.txt"
    title = "Joseh's store"
    transaction_type = "outcome"
    value = "40.5"
    post_transaction(filename, title, transaction_type, value)

def test_transaction_list():
    filename = "transactions.txt"
    expected = [
        {'title': 'supermarket purchases', 'transaction_type': 'outcome', 'value': '400.85'},
        {'title': 'salary', 'transaction_type': 'income', 'value': '3000'},
        {'title': "Joseh's store", 'transaction_type': 'outcome', 'value': '40.5'}
    ]

    transaction_list = all_transaction(filename)
    #assert transaction_list == expected

def test_make_appointment():
    filename = "psychologist_appointments.txt"
    name = "Renato da Cruz"
    hour = "2020-08-29 14:00:00"
    description = "me sinto muito estressado com a rotina"
    make_appointment(filename, name, hour, description) 
    
    filename = "psychologist_appointments.txt"
    name = "Leandro Costa"
    hour = "2020-10-15 9:00:00"
    description = "ultimamente tenho feito algumas dietas pra tentar emagrecer na quarentena mas nao tem dado certo"
    make_appointment(filename, name, hour, description) 

    filename = "psychologist_appointments.txt"
    name = "Cassiana Pereira"
    hour = "2020-09-25 11:00:00"
    description = "sou muito ansiosa por natureza, mas na quarentena isso tem piorado cada vez mais"
    make_appointment(filename, name, hour, description) 

def test_time_is_free():
    filename = "psychologist_appointments.txt"
    hour1 = "2020-09-25 10:00:00"
    result = time_is_free(filename, hour1)
    print(result)